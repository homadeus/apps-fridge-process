#ifndef HOMADEUS_PROCESSES_FRIDGE_PROCESS_H // in order to acknowledge only once
#define HOMADEUS_PROCESSES_FRIDGE_PROCESS_H


#include <process.h> // define process begin etc...
#include <etimer.h>

extern double fridge_get_mean();
extern unsigned long fridge_get_up_time();
extern unsigned long fridge_get_total_time();

#define HOMADEUS_FRIDGE_PROCESS_DESCRIPTION "Homadeus fridge process"
#define HOMADEUS_FRIDGE_PROCESS hmd_fridge_process
#define HOMADEUS_FRIDGE_EVENT hmd_fridge_event

/* Definition of the fridge process */
PROCESS_NAME(HOMADEUS_FRIDGE_PROCESS);

#endif /* end of include guard: HOMADEUS_PROCESSES_FRIDGE_PROCESS_H */
