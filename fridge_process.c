/*
 * Homadeus Platform TWI Driver
 * Copyright (C) 2013 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/clock.h>
#include <homadeus/processes/fridge_process.h>
#include <homadeus/devices/78M6610.h>
#include <homadeus/utils/utils.h>



unsigned long total_time = 0;
unsigned long up_time = 0;
double mean = 0;

double fridge_get_mean(){

	return mean;
}

unsigned long fridge_get_up_time(){

	return up_time;
}

unsigned long fridge_get_total_time(){

	return total_time;
}


#define POWER_ON_THRESHOLD_WATTS 3 
int current_state = 0 ; // State OFF ==0,  STATE UP == 1
int last_state = 0;

unsigned long start_time = 0;
double accum_power = 0; // beware of possible overflow
unsigned long nr_samples =0;

double get_instant_power()
{
	double power = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_P);
	return power;
}

int get_current_state()
{
    
    if( get_instant_power() <= POWER_ON_THRESHOLD_WATTS ) return 0;
    else return 1;
}





PROCESS(hmd_fridge_process, HOMADEUS_FRIDGE_PROCESS_DESCRIPTION);

/*Implementation of the fridge process */
PROCESS_THREAD(hmd_fridge_process, ev, data) {

  static struct etimer timer;
   
  PROCESS_BEGIN();
  
// first run , we initialize some variables
  start_time = clock_seconds();

  while(1){
	
	etimer_set(&timer, CLOCK_CONF_SECOND / 4);  // Te period of the algorithm will be 1/4 of a second.
	
	PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timer));
	
	current_state = get_current_state();
	
	if(current_state == 1){

		if(last_state == 0){
			nr_samples = 1;
			accum_power = get_instant_power();
			
			total_time = clock_seconds() - start_time;
			
			start_time = clock_seconds(); 
		}

		if (last_state == 1){
			accum_power += get_instant_power();
			nr_samples += 1;
		}
	}
	if (current_state == 0){
		if(last_state == 1){

			mean = accum_power / (double)nr_samples;
			up_time = clock_seconds() - start_time;
			
		}
		//if(last_state == 0) //we do nothing
		
	}

	last_state = current_state;
 }
 PROCESS_END();
}

